import React, { useState, useEffect } from 'react';
import { Button, Space, Typography } from 'antd';
import { HomeOutlined } from '@ant-design/icons';
import 'antd/dist/antd.min.css';

import './App.css'
import StreetPicker from './components/StreetPicker';
import BinDays from './components/BinDays';

const { Link } = Typography;
const BIN_DAYS_LOCAL_STORAGE_KEY = "sunny-coast-bin-days";

function App() {

    const [settings, setSettings] = useState(JSON.parse(localStorage.getItem(BIN_DAYS_LOCAL_STORAGE_KEY)));
    const [isModalVisible, setIsModalVisible] = useState(settings == null);

    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    const handleOk = (selectedSettings) => {
        setSettings(selectedSettings);
        setIsModalVisible(false);
    }
    
    useEffect(() => {
        if(settings){
            localStorage.setItem(BIN_DAYS_LOCAL_STORAGE_KEY, JSON.stringify(settings))
        }
    }, [settings]);

    return (
        <>
            <div className="App" style={{ textAlign: 'center' }}>
                <BinDays settings={settings} /> 
                <div className="footer">
                    <Space direction="vertical">
                        <Button type="primary" icon={<HomeOutlined />} onClick={showModal}>
                            Settings
                        </Button>
                        <Link href="https://www.sunshinecoast.qld.gov.au/Living-and-Community/Waste-and-Recycling/Bin-collection-days" target="_blank">
                            powered by Sunshine Coast Council open data
                        </Link>
                    </Space>
                </div>
            </div>
            <StreetPicker isModalVisible={isModalVisible} onCancel={handleCancel} onOk={handleOk} />
        </>
    );
}

export default App;
