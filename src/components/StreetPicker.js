import React, { useEffect, useState } from 'react';
import { Modal, Select, Space } from 'antd';

const { Option } = Select;

const StreetPicker = ({ isModalVisible, onCancel, onOk }) => {

    const [suburbs, setSuburbs] = useState(null);
    const [streets, setStreets] = useState(null);
    const [selectedSuburb, setSelectedSuburb] = useState(null);
    const [selectedStreet, setSelectedStreet] = useState(null);


    const onChangeSuburb = (value) => {
        setSelectedSuburb(value);
    };

    const onChangeStreet = (value) => {
        setSelectedStreet(value);
    };

    const onSave = () => {
        onOk({
            street: selectedStreet,
            suburb: selectedSuburb
        });
    }

    useEffect(() => {
        if(selectedSuburb){
            // load streets
            fetch(`https://www.sunshinecoast.qld.gov.au/data/Directory/Streets?suburb=${selectedSuburb}`)
                .then(res => res.json())
                .then(json => {
                    setStreets(json);
                })
                .catch(err => console.error("unexpected error", err));
        }
    }, [selectedSuburb]);

    useEffect(() => {
        if (isModalVisible){
            // load suburbs
            fetch("https://www.sunshinecoast.qld.gov.au/data/Directory/Suburbs")
                .then(res => res.json())
                .then(json => {
                    setSuburbs(json);
                })
                .catch(err => console.error("unexpected error", err));
        }
    }, [isModalVisible]);

    return (
        <>
            <Modal
                title="Find your Suburb & Street"
                centered
                visible={isModalVisible}
                width={250}
                okText="Save"
                onOk={onSave}
                okButtonProps={{disabled: selectedStreet == null}}
                onCancel={onCancel}
            >
                <Space direction="vertical">
                    <Select
                        showSearch
                        placeholder="Select a Suburb"
                        optionFilterProp="children"
                        onChange={onChangeSuburb}
                        style={{ minWidth: 180 }}
                        filterOption={(input, option) =>
                            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                        }
                    >
                        {
                            suburbs && suburbs.map((suburb) => {
                                return (<Option key={suburb} value={suburb}>{suburb}</Option>)
                            })
                        }
                    </Select>
                    <Select
                        showSearch
                        placeholder="Select a Street"
                        optionFilterProp="children"
                        onChange={onChangeStreet}
                        disabled={selectedSuburb == null}
                        style={{ minWidth: 180 }}
                        filterOption={(input, option) =>
                            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                        }
                    >
                        {
                            streets && streets.map((street) => {
                                return (<Option key={street.Name} value={street.Name}>{street.Name}</Option>)
                            })
                        }
                    </Select>
                </Space>
            </Modal>
        </>
    );
};

export default StreetPicker;