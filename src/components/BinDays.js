import React, { useEffect, useState } from 'react';
import { Space, List } from 'antd';
import { RestTwoTone, ClockCircleOutlined } from '@ant-design/icons';
import moment from 'moment';

const BinDays = ({ settings }) => {

    const [days, setDays] = useState(null);

    const getBinColour = (collectionType) => {
        const red = '#bf1d1b';
        const yellow = '#f2c400';
        const green = '#7a9a01';
        const black = '#000000';
        let colour;
        switch(collectionType) {
            case "General waste":
                colour = red;
                break;
            case "Recycling":
                colour = yellow;
                break;
            case "Garden waste":
                colour = green;
                break;
            default:
                colour = black;

        }
        return colour;
    };

    const getDateAndDays = (timestampStr) => {
        const m = moment(timestampStr).startOf('day');
        const date = m.format('Do MMM YYYY');
        const inDays = m.diff(moment().startOf('day'), 'days');
        if(inDays === 0) { 
            return `${date} (today)`;
        }
        return `${date} (in ${inDays} days)`;
    }

    const processDays = (councilJson) => {
        councilJson[0].Days.sort(
            (a,b) => {
            return new Date(a.CollectionDay) - new Date(b.CollectionDay);
        });
        return councilJson[0].Days;
    };

    useEffect(() => {
        if(settings){
            // load the actual bin days
            fetch(`https://www.sunshinecoast.qld.gov.au/data/Directory/BinCollectionResults?Name=${settings.street}&Suburb=${settings.suburb}`)
                .then(res => res.json())
                .then(json => {
                    setDays(processDays(json));
                })
                .catch(err => console.error("unexpected error", err));
        }
    }, [settings]);

    return (
        <>
            <Space direction="vertical">
                {
                    settings == null || days == null  ? 
                        <>
                            <h2>loading...</h2>
                        </>
                    : <>
                            <h3>Sunshine Coast - Bin Collection Days</h3>
                            <h2>{settings.street}, {settings.suburb}</h2>
                            <List
                                itemLayout='horizontal'
                            >
                                <List.Item>
                                    <List.Item.Meta
                                    avatar={<RestTwoTone style={{fontSize: 18}} twoToneColor={getBinColour(days[0].Type)} />}
                                    title={days[0].Type}
                                    description={getDateAndDays(days[0].CollectionDay)}
                                    />
                                </List.Item>
                                {days[0].CollectionDay.split('T')[0] !== days[1].CollectionDay.split('T')[0] && (<List.Item>
                                    <List.Item.Meta
                                        avatar={<ClockCircleOutlined  style={{fontSize: 18}}/>}
                                        title="..."
                                        />
                                </List.Item>)}
                                <List.Item>
                                    <List.Item.Meta
                                    avatar={<RestTwoTone style={{fontSize: 18}} twoToneColor={getBinColour(days[1].Type)} />}
                                    title={days[1].Type}
                                    description={getDateAndDays(days[1].CollectionDay)}
                                    />
                                </List.Item>
                                {days[1].CollectionDay.split('T')[0] !== days[2].CollectionDay.split('T')[0] && (<List.Item>
                                    <List.Item.Meta
                                        avatar={<ClockCircleOutlined  style={{fontSize: 18}}/>}
                                        title="..."
                                        />
                                </List.Item>)}
                                <List.Item>
                                    <List.Item.Meta
                                    avatar={<RestTwoTone style={{fontSize: 18}} twoToneColor={getBinColour(days[2].Type)} />}
                                    title={days[2].Type}
                                    description={getDateAndDays(days[2].CollectionDay)}
                                    />
                                </List.Item>
                            </List>
                            {days.Note && <h3>Note: {days.Note}</h3>}
                        </>
                }
            </Space>
        </>
    );
};

export default BinDays;